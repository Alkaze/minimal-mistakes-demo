---
title: "About"
permalink: /about/
layout: single
author_profile: true
---

A public [Staticman][sm] API instance to demonstrate its
[native GitLab support][gitlab_supp].

| "name" | "public GitLab instance" | "public Framagit instance" |
| --- | --- | --- |
| GitLab instance served | GitLab.com | Framagit |
| `gitlabBaseUrl` | https://gitlab.com | https://framagit.org |
| GitLab Bot | [staticmanlab](https://gitlab.com/staticmanlab) | [staticmanlab1](https://framagit.org/staticmanlab1) |
| `githubBaseUrl` | https://github.com | https://github.com |
| API URL | https://staticman3.herokuapp.com | https://staticman-frama.herokuapp.com |

Here's a [list of Staticman demo sites on Framagit][demo_list].

[sm]: https://staticman.net
[gitlab_supp]: https://github.com/eduardoboucas/staticman/pull/219
[demo_list]: https://framagit.org/staticman-gitlab-pages
